export default class Pyro {
  constructor(canvas, ctx){
    this.ctx = ctx
    this.canvas = canvas
    this.speed = 0
    this.velocity = Math.random() * 0.5
    this.position1 = Math.floor(this.randomY)
    this.position2 = Math.floor(this.randomX)
    this.randomSize = this.getNumberRandom(2, 4)
    this.x= this.getNumberRandom(0, this.canvas.width)
    this.y= this.canvas.height
    this.topRandom = this.getNumberRandom((this.canvas.height / 3), (this.canvas.height / 2))
    this.redRandom = this.getNumberRandom(1,255)
    this.greenRandom = this.getNumberRandom(1,255)
    this.blueRandom = this.getNumberRandom(1,255)
    this.sizeExplosion = 80
  }
  update(){
    if(this.y < (this.topRandom + 1)) {
      this.randomSize += 0.09

      this.ctx.beginPath()
      this.ctx.arc(this.x, this.y, (this.randomSize * 20), 0, Math.PI * 2)
      this.ctx.fillStyle= 'rgba('+this.redRandom +','+ this.greenRandom + ',' + this.blueRandom+','+1+')'
      this.ctx.shadowBlur = 10
      this.ctx.fill()
      this.ctx.closePath()

      this.ctx.beginPath()
      this.ctx.arc(this.x, this.y, (this.randomSize * 40), 0, Math.PI * 2)
      this.ctx.shadowBlur = 10
      this.ctx.strokeStyle = '#fff'
      this.ctx.stroke()
      this.ctx.closePath()
    }
    if(this.y <= this.topRandom){
      this.y = this.canvas.height + this.randomSize
      this.x = this.getNumberRandom(0, this.canvas.width)
      this.randomSize = this.getNumberRandom(2,4)
    }
    this.y -= this.getNumberRandom(3, 5)
    this.randomSize
  }
  getNumberRandom(min, max){
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
  
  draw(){
    this.ctx.beginPath()
    this.ctx.arc( this.x, this.y, this.randomSize, 0, 2 * Math.PI)
    this.ctx.fillStyle= 'rgba('+this.redRandom +','+ this.greenRandom + ',' + this.blueRandom+','+1+')'
    this.ctx.fill()
    this.ctx.shadowBlur = 10
    this.ctx.closePath()
  }

  drawExplosion(){
    this.ctx.beginPath()
    this.ctx.arc(100, 100, 20, 0, 2 * Math.PI)
    this.ctx.fill()
    this.ctx.fillStyle = '#fff'
    this.ctx.shadowBlur = 10
    this.ctx.closePath()
  }
}
