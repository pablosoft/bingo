import { GetterTree, ActionTree, MutationTree } from "vuex"

interface Bolas {
  id: number,
  value: boolean
}

export const state = () => ({
  listBalls: [] as Bolas[],
  listBallsWinners: [] as Bolas[],
  listBallsTombola: [] as Bolas[],
  numMin: 1 as number,
  numMax: 90 as number,
  numWinner: 0 as number,
  isDialog: false as boolean,
  isLine: false as boolean,
  isBingo: false as boolean
})

export type RootState= ReturnType<typeof state>

// export const getters: GetterTree<BallModuleState, BallModuleState> = {
//   evenMore: state => state.listBalls + 5
// }

export const getters: GetterTree<RootState, RootState> = {
  getListBallsTombola(state) {
    return state.listBalls.filter(item => item.value === false)
  },
  getListBallsWinners(state) {
    return state.listBalls.filter(item => item.value === true)
  }
}
// export const getters: GetterTree<RootState, RootState> = {
//   count: state => state.count
// }

export const mutations: MutationTree<RootState> = {
  ADD_BALL(state, newVal: Bolas) {
    state.listBalls.push(newVal)
  },

  CHANGE_NUMMAX( state, newVal:number ) {
    state.numMax = newVal
  },

  CREATE_LISTBALLS( state ) {
    state.listBalls = []
    for(let i = 0; i < state.numMax; i++) {
      state.listBalls.push({id: i + state.numMin, value: false})
    }
  },
  UPDATE_LISTBALLS(state, newVal) {
    state.listBalls = newVal
  },

  ADD_LISTBALLSWINNER(state, newVal:Bolas) {
    state.listBallsWinners.push(newVal)
  },
  UPDATE_LISTBALLSWINNER(state, newVal) {
    state.listBallsWinners = newVal
  },
  CLEAN_LISTBALLSWINNER(state) {
    state.listBallsWinners = []
  },

  ADD_LISTBALLSTOMBOLA(state,newVal:Bolas){
    state.listBallsTombola.push(newVal)
  },
  UPDATE_LISTBALLSTOMBOLA(state, newVal){
    state.listBallsTombola = newVal
  },
  CREATE_LISTBALLSTOMBOLA(state) {
    console.log('crear listBallsTombola')
  },
  CLEAN_LISTBALLSTOMBOLA(state) {
    state.listBallsTombola = []
  },
  UPDATE_ITEMLISTBALLSTOMBOLA(state) {
    for(let i = 0; i < state.listBalls.length; i++) {
      if(state.numWinner === state.listBalls[i].id) {
        state.listBalls[i].value = true

      }
    }
  },
  CHANGE_NUMWINNER(state, newVal: number) {
    state.numWinner = newVal
  },
  
  UPDATE_NUMWINNER(state, newVal:number) {
    state.numWinner = newVal 
  },

  CHANGE_ISDIALOG(state, newVal: boolean) {
    state.isDialog = newVal
  },

  CHANGE_ISLINE(state, newVal: boolean) {
    state.isLine = newVal
  },

  CHANGE_ISBINGO(state, newVal: boolean) {
    state.isBingo = newVal
  }

}

