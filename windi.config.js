export default {
  extract: {
    include: [
      'components/**/*.{vue,ts}',
      'pages/**/*.{vue,ts}',
      'layouts/**/*.{vue,ts}'
    ],
  },
  darkMode: 'class',
  theme: {
    extend: {
      backgroundImage: theme => ({
        'bingo': 'url(\'~/static/wallpaperbingo-solid.png\')',
      }),
      boxShadow: theme => ({
        'bottom-sm': '0 4px rgba(200, 200, 200, 1)',
        'bottom-md': '0 9px rgba(200, 200, 200, 1)',
      }),
      fontFamily: {
        'modak': ['"Modak"', 'cursive'],
        'fredoka': ['"Fredoka-One"', 'cursive'],
      },
      textShadow: {
        'font': '2px 2px rgba(0,0,0)'
      }
    },
  },
  shortcuts: {
    'card': {
      '@apply': 'text-left block border border-solid border-gray-200 rounded overflow-hidden'
    },
    'btn-3d': {
      '@apply': [
        'inline-block rounded-lg outline-none appearance-none',
        'bg-red-600 hover:bg-red-500 active:bg-red-400',
        'shadow-bottom-md active:shadow-bottom-sm focus:outline-none',
        'transform active:translate-y-4px',
        'text-lg text-center text-white font-fredoka text-shadow-font no-underline',
        'px-4 py-2',
      ],
    },
  }
}
